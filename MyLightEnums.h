
#ifndef MyLightEnums_h
#define MyLightEnums_h

#include <Arduino.h>

//class MyLightEnums
//{
//  public:
  
    typedef enum MenuState { music, solid, pattern,   //top level
                             pulse, fadeMusic, cycleMusic,   //music
                             red, blue, green,          //solid
                             fade, cycle, strobe        //pattern
                           } MenuState_t;

    typedef enum MenuInput{ left, right, select }MenuInput_t;

    typedef struct{
    MenuState_t currentState;
    MenuInput_t input;
    MenuState_t nextState;
    } menu_transition_t;

    // properties
    typedef enum ButtonState{bh1 = 0,bh2 = 1,bh3 = 2,active = 3,bl1 = 4,bl2 = 5,bl3 = 6, passive = 7} ButtonState_t;

	typedef enum Input{high,low} Input_t;

	typedef struct {
	ButtonState_t currentState;
	Input_t input;
	ButtonState_t nextState;
	} transition_t;
	
//};

#endif
