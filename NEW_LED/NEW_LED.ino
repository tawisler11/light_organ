/*************************************************************
 *    Autor: Taylor Wisler
 *    Date Edited: 8/28/2014
 *    Purpose: Control LED strip and LCD display
 *************************************************************
 */

/*************************************************************
 *                       Includes
 *************************************************************
 */
#include "Arduino.h"
#include <LiquidCrystal.h>
#include "MyLightEnums.h"
#include "TimerOne.h"

/*************************************************************
 *               Constants and Declarations
 *************************************************************
 */
LiquidCrystal lcd(12, 11, 2, 4, 7, 8);
//                 (rs, E,d4,d5,d6,d7)

#define NUM_TRANS 16
#define NUM_MODES 4
#define NUM_MENU_STATE 36 //3 * #MenuState_t values
#define COLOR_LENGTH 6
#define PULSE_COLORS 6
#define PULSE_COLOR_HITS 20

int led_red = 3;
int led_blue = 5;
int led_green = 6; 
int left_button = 0;
int right_button = 1;
int select_button = 2;
int bass_pin = 3;

int pulseColorStateColor = 0;
int pulseColorState = 0;
int pulseState = 0;
int cycleState = 0;
int modeState = 0;
int modeCounter = 0;
int ledStateCounter = 0;
int intensity = 0;
int i = 0;
int j = 0;

boolean needsAction = false;
boolean bassHit = false;

void (*current_mode)(void);

int bassRead = 0;
static int bassCounter = 0;
const int bassConstant = 1020;


MenuState_t menuState = music;
MenuState_t currentLightState = fade;

menu_transition_t Menu_STT[NUM_MENU_STATE] = {
  {music,       left,      pattern,      play_nothing},
  {music,       right,     solid,        play_nothing},
  {music,       select,    pulse,        play_nothing},
  {solid,       left,      music,        play_nothing},
  {solid,       right,     pattern,      play_nothing},
  {solid,       select,    RED,          play_nothing},
  {pattern,     left,      solid,        play_nothing},
  {pattern,     right,     music,        play_nothing},
  {pattern,     select,    fade,         play_nothing},
  {pulse,       left,      cycleMusic,   play_nothing},
  {pulse,       right,     fadeMusic,    play_nothing},
  {pulse,       select,    music,        play_pulse},
  {fadeMusic,   left,      pulse,        play_nothing},
  {fadeMusic,   right,     cycleMusic,   play_nothing},
  {fadeMusic,   select,    music,        play_fadeMusic},
  {cycleMusic,  left,      fadeMusic,    play_nothing},
  {cycleMusic,  right,     pulse,        play_nothing},
  {cycleMusic,  select,    music,        play_cycleMusic},
  {RED,         left,      GREEN,        play_nothing},
  {RED,         right,     BLUE,         play_nothing},
  {RED,         select,    music,        play_RED},
  {BLUE,        left,      RED,          play_nothing},
  {BLUE,        right,     GREEN,        play_nothing},
  {BLUE,        select,    music,        play_BLUE},
  {GREEN,       left,      BLUE,         play_nothing},
  {GREEN,       right,     RED,          play_nothing},
  {GREEN,       select,    music,        play_GREEN},
  {fade,        left,      strobe,       play_nothing},
  {fade,        right,     cycle,        play_nothing},
  {fade,        select,    music,        play_fade},
  {cycle,       left,      fade,         play_nothing},
  {cycle,       right,     strobe,       play_nothing},
  {cycle,       select,    music,        play_cycle},
  {strobe,      left,      cycle,        play_nothing},
  {strobe,      right,     fade,         play_nothing},
  {strobe,      select,    music,        play_strobe},
};

ButtonState_t leftButton = passive;
ButtonState_t rightButton = passive;
ButtonState_t selectButton = passive;

ButtonState_t bassState = passive;

transition_t STT1[NUM_TRANS] = {
	{bh1,     low,  passive},
	{bh1,     high, bh2},
	{bh2,     low,  passive},
	{bh2,     high, bh3},
	{bh3,     low,  active},
	{bh3,     high, active},
	{active,  low,  bl1},
	{active,  high, active},
	{bl1,     low,  bl2},
	{bl1,     high, active},
	{bl2,     low,  bl3},
	{bl2,     high, active},
	{bl3,     low,  passive},
	{bl3,     high, passive},
	{passive, low,  passive},
	{passive, high, bh1}
};

/*************************************************************
 *                       Color Arrays
 *************************************************************
 */
int red[3] = {255,0,0};
int green[3] = {0,255,0};
int blue[3] = {0,0,255};

int cyan[3] = {0,255,255};
int yellow[3] = {255,255,0};
int violet[3] = {255,0,255};

int orange[3] = {255,128,0};
int lightGreen[3] = {128,255,0};
int aqua[3] = {0,255,128};
int babyBlue[3] = {0,128,255};
int purple[3] = {127,0,255};
int magenta[3] = {255,0,127};

int white[3] = {255,255,255};
int black[3] = {0,0,0};
int gray[3] = {128,128,128};

/*************************************************************
 *                       Color Patterns
 *************************************************************
 */
 
int colors_pattern[6][3] = 
{
 {cyan[0],cyan[1],cyan[2]},
 {green[0],green[1],green[2]},
 {blue[0],blue[1],blue[2]},
 {red[0],red[1],red[2]},
 {yellow[0],yellow[1],yellow[2]},
 {violet[0],violet[1],violet[2]}
};

int rbg_pattern[6][3] = 
{
 {0,1,1},
 {0,1,0},
 {0,0,1},
 {1,0,0},
 {1,1,0},
 {1,0,1}
};

/*************************************************************
 *                       Pin Array
 *************************************************************
 */
 
int pins[3] =
{
 led_red,
 led_green,
 led_blue 
};

int pins_pulse[3] =
{
 led_blue,
 led_red,
 led_green
};

/*************************************************************
 *                      Button State Array
 *************************************************************
 */
ButtonState_t buttonArray[] = 
{
 leftButton,
 rightButton,
 selectButton 
};

/*************************************************************
 *                      String Array
 *************************************************************
 */
 String displayStringArray[] = 
 {
  "Music",
  "Solid",
  "Pattern",
  "Pulse to Music",
  "Fade to Music",
  "Cycle to Music",
  "Red",
  "Blue",
  "Green",
  "Fade",
  "Cycle",
  "Strobe"
 };

/*************************************************************
 *                       Function Pointer Arrays
 *************************************************************
 */
//===============================
//          Buttons
//===============================
void (*buttonLeft_FncPtr[])(void) = {  
  
  nothing_action,//bh1
  nothing_action,//bh2
  buttonLeftAction,//bh3
  nothing_action,//active
  nothing_action,//bl1
  nothing_action,//bl2
  nothing_action,//bl3
  nothing_action//passive
	
};

void (*buttonRight_FncPtr[])(void) = {  
  
  nothing_action,//bh1
  nothing_action,//bh2
  buttonRightAction,//bh3
  nothing_action,//active
  nothing_action,//bl1
  nothing_action,//bl2
  nothing_action,//bl3
  nothing_action//passive
	
};

void (*buttonSelect_FncPtr[])(void) = {  
  
  nothing_action,//bh1
  nothing_action,//bh2
  buttonSelectAction,//bh3
  nothing_action,//active
  nothing_action,//bl1
  nothing_action,//bl2
  nothing_action,//bl3
  nothing_action//passive
	
};

//===============================
//          Menu
//===============================

void (*menuAction_FncPty[])(void) = {
  nothing_action,
  playPulse,
  playFadeMusic,
  playCycleMusic,
  playRED,
  playBLUE,
  playGREEN,
  playFade,
  playCycle,
  playStrobe
};

//===============================
//          Music
//===============================
void (*bassHit_FncPtr[])(void) = {  
  
  nothing_action,//bh1
  nothing_action,//bh2
  bassHitAction,//bh3
  nothing_action,//active
  nothing_action,//bl1
  nothing_action,//bl2
  nothing_action,//bl3
  nothing_action//passive
	
};

/*************************************************************
 *                       ISR Function
 *************************************************************
 */
void interruptFunction(){
  needsAction = true;
}


/*************************************************************
 *                       Setup Function
 *************************************************************
 */
 
void setup() {                
  // initialize the digital pin as an output.
  lcd.begin(20, 4);
  writeToScreen(menuState);
  
  pinMode(led_red, OUTPUT);
  pinMode(led_blue, OUTPUT);
  pinMode(led_green, OUTPUT);  
  analogWrite(led_blue,255);
  analogWrite(led_red,0);
  analogWrite(led_green,0);
  
  Timer1.initialize(5000); //5000 for fade, 500000 for cycle
  Timer1.attachInterrupt(interruptFunction);
  
  menuState = fade;
  playFade();
  menuState = music;
}

/*************************************************************
 *                   Main loop Begin
 *************************************************************
 */
 
void loop() 
{
  if(needsAction){
    ledTask();
    needsAction = false;
  }
  buttonTask();  
} // End of Main


void menuFsm( MenuInput_t in ){
  
  if(in == select){
    void (*selected_action)(void);
    MenuAction_t temp_action = lookup_menuAction( menuState, in, Menu_STT );
    selected_action = menuAction_FncPty[temp_action];
    selected_action();
  }
  
  menuState = lookup_menuTransition( menuState, in, Menu_STT );
  
  writeToScreen(menuState);
  
  return;
}

void writeToScreen( MenuState_t q0){
  
  lcd.clear();
  lcd.print(displayStringArray[q0]);
  lcd.setCursor(0,3);
  lcd.print(displayStringArray[currentLightState]);
  return;
}

MenuAction_t lookup_menuAction(MenuState_t q0, MenuInput_t in, menu_transition_t MSTT[]){
  for(int i = 0; i < NUM_MENU_STATE; i++){
    if(MSTT[i].currentState == q0 && MSTT[i].input == in)
      return MSTT[i].menuAction;
  }
}

MenuState_t lookup_menuTransition(MenuState_t q0, MenuInput_t in, menu_transition_t MSTT[]){
  for(int i = 0; i < NUM_MENU_STATE; i++){
      if(MSTT[i].currentState == q0 && MSTT[i].input == in)
        return MSTT[i].nextState;
  }
}

void playPulse( void ){
  
  Timer1.setPeriod(1000);
  current_mode = pulseBlueBass;
  currentLightState = menuState;
}
void playFadeMusic( void ){
  //TODO
}
void playCycleMusic( void ){
  
  Timer1.setPeriod(1000);
  current_mode = switchColorsBass;
  currentLightState = menuState;
}
void playRED( void ){
  
  Timer1.setPeriod(1000000);
  current_mode = nothing_action;
  currentLightState = menuState;
  
  analogWrite(led_blue,0);
  analogWrite(led_red,255);
  analogWrite(led_green,0);
  
}
void playBLUE( void ){
  
  Timer1.setPeriod(1000000);
  current_mode = nothing_action;
  currentLightState = menuState;
  
  analogWrite(led_blue,255);
  analogWrite(led_red,0);
  analogWrite(led_green,0);
  
}
void playGREEN( void ){
  
  Timer1.setPeriod(1000000);
  current_mode = nothing_action;
  currentLightState = menuState;
  
  analogWrite(led_blue,0);
  analogWrite(led_red,0);
  analogWrite(led_green,255);
  
}
void playFade( void ){
  
  Timer1.setPeriod(5000);
  current_mode = fadeColors;
  currentLightState = menuState;
  
}
void playCycle( void ){
  
  Timer1.setPeriod(500000);
  current_mode = cycleColors;
  currentLightState = menuState;
  
}
void playStrobe( void ){
  //TODO
}

/*************************************************************
 *                      Button Functions
 *************************************************************
 */

void buttonCheck(ButtonState_t &currentButton, int buttonPin, void (*moore_table[])(void)){
  Input_t input;
  int buttonRead = analogRead(buttonPin);
  if(buttonRead > 600) input = high;
  else input = low; 
  
  currentButton = fsm(currentButton, STT1, input, moore_table);
} 

void buttonTask( void )
{
  buttonCheck(leftButton, left_button, buttonLeft_FncPtr); 
  buttonCheck(rightButton, right_button, buttonRight_FncPtr);
  buttonCheck(selectButton, select_button, buttonSelect_FncPtr);
} 

ButtonState_t lookup_transition(ButtonState_t q0,Input_t in, transition_t STT[]){
  for(int i = 0; i < NUM_TRANS; i++){
    if(STT[i].currentState == q0 && STT[i].input == in)
    return STT[i].nextState; 
  }
  return q0;
}

void buttonSelectAction( void )
{
  menuFsm(select);
}

void intteruptSpeedSelect( void )
{
  if(modeState == 0)
    Timer1.setPeriod(5000);
  else if(modeState == 1)
    Timer1.setPeriod(500000);
  else if(modeState == 2)
    Timer1.setPeriod(1000);
  else if(modeState == 3)
    Timer1.setPeriod(1000);
}

void buttonLeftAction( void )
{
  menuFsm(left);
}
void buttonRightAction( void )
{
  menuFsm(right);
}
void nothing_action( void )
{
  return;
}

/*************************************************************
 *                    Light Task
 *************************************************************
 */

void ledTask( void )
{
  current_mode();
}

/*************************************************************
 *                    Light Functions
 *************************************************************
 */
void fadeColors( void )
{
  if(ledStateCounter == 0){
    fadeUp(led_green);
    return;
  }
  else if(ledStateCounter == 1){
    fadeDown(led_blue);
    return;
  }
  else if(ledStateCounter == 2){
    fadeUp(led_red);
    return;
  }
  else if(ledStateCounter == 3){
    fadeDown(led_green);
    return;
  }
  else if(ledStateCounter == 4){
    fadeUp(led_blue);
    return;
  }
  else if(ledStateCounter == 5){
    fadeDown(led_red);
    return;
  }
}

void fadeDown(int color)
{
   intensity--;
   analogWrite(color, intensity);
   if(intensity <= 1)
   {
    ledStateCounter++; 
   }
   if(ledStateCounter > 5)
   {
    ledStateCounter = 0; 
   }
}

void fadeUp(int color)
{
   intensity++;
   analogWrite(color, intensity);
   if(intensity >= 255)
   {
    ledStateCounter++; 
   }
   if(ledStateCounter > 5)
   {
    ledStateCounter = 0; 
   }
}

void cycleColors( )//int colors[][3] )
{
   cycleState++;
   if(cycleState >= COLOR_LENGTH)
     cycleState = 0;
     
   for(j=0;j<3;j++)
   {
    analogWrite(pins[j],colors_pattern[cycleState][j]);  
   }
}

void switchColorsBass()//int colors[][3])
{
  bassCheck();
  
  if( bassHit )
  {
    bassHit = false;
    
    analogWrite(pins[0],colors_pattern[bassCounter][0]);
    analogWrite(pins[1],colors_pattern[bassCounter][1]);
    analogWrite(pins[2],colors_pattern[bassCounter][2]);
    bassCounter++;
    if(bassCounter > 5) bassCounter = 0;
  }
}

void pulseBlueBass()
{
  bassCheck();
  
  if( bassHit )
  {
    bassHit = false;
    pulseColorState++;
    
    intensity = 255;
    
    if(pulseColorState >= PULSE_COLOR_HITS)
    {
      pulseColorState = 0;
      pulseColorStateColor++;
      if(pulseColorStateColor >= PULSE_COLORS)
        pulseColorStateColor = 0; 
      analogWrite(pins[0], 0);
      analogWrite(pins[1], 0);
      analogWrite(pins[2], 0);
    }
    analogWrite(pins[0],(rbg_pattern[pulseColorStateColor][0]*intensity));
    analogWrite(pins[1],(rbg_pattern[pulseColorStateColor][1]*intensity));
    analogWrite(pins[2],(rbg_pattern[pulseColorStateColor][2]*intensity));
  } 
  else
  {
    pulseState++;
    if(pulseState >= 4)
    {
      if(intensity > 0)
      {
        intensity--;
        analogWrite(pins[0],(rbg_pattern[pulseColorStateColor][0]*intensity));
        analogWrite(pins[1],(rbg_pattern[pulseColorStateColor][1]*intensity));
        analogWrite(pins[2],(rbg_pattern[pulseColorStateColor][2]*intensity));
      }
      pulseState = 0;
    }
  }
}

void bassHitAction( void ){
 bassHit = true; 
}

void bassCheck( void ){
  Input_t input;
  int bassRead = analogRead(bass_pin);
  if(bassRead > bassConstant) input = high;
  else input = low; 
  
  bassState = fsm(bassState, STT1, input, bassHit_FncPtr);
} 


